#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

if [[ -n "${CI_JOB_ID}" ]]; then
    rm -rf "${CI_PROJECT_DIR:-${PWD}}"/zfs_autobackup
fi

script_dir="$(dirname "$(readlink -f "$0")")"
. "${script_dir}"/vars

cd "${CI_PROJECT_DIR:-${PWD}}"/zfs_autobackup

# fix imports
sed \
    -i \
    -e 's,^from \.\s,from zfs_autobackup ,' \
    -e 's,^from \.\(\w.*\),from zfs_autobackup.\1,' \
    zfs_autobackup/*.py

declare -A programs=()

programs["zfs-autobackup"]="ZfsAutobackup"

if [[ "${version}" =~ 3\.2.* ]] || [[ "${patchlevel}" =~ v3\.2.* ]] ; then
    programs["zfs-autoverify"]="ZfsAutoverify"
    programs["zfs-check"]="ZfsCheck"
fi

for program in "${!programs[@]}"; do
    file="${programs[${program}]}.py"
    /root/.pyenv/shims/nuitka3 \
        --onefile \
        --include-package=zfs_autobackup \
        --remove-output \
        -o "${program}" \
        zfs_autobackup/"${file}"
    mv "${program}" ../
done
cd -
