#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
. "${script_dir}"/vars

architecture="$(dpkg --print-architecture)"
description="ZFS autobackup is used to periodicly backup ZFS filesystems to other locations. Easy to use and very reliable."
url="https://github.com/psy0rz/zfs_autobackup"

apt-get -qq update
apt-get -qqy install ruby-dev
gem install --quiet --no-document fpm

programs=()
programs+=("zfs-autobackup")
programs+=("zfs-autoverify")
programs+=("zfs-check")

mkdir -p "${distribution}"

for program in "${programs[@]}"; do
    fpm \
        --force \
        --log "${fpm_loglevel:-error}" \
        --architecture "${architecture}" \
        --license "GPL-3.0" \
        --vendor "Edwin Eefting <edwin@datux.nl>" \
        --input-type dir \
        --output-type deb \
        --package "${distribution}/${program}_${version}+${patchlevel}_${architecture}.deb" \
        --name "${program}" \
        --version "${version}+${patchlevel}" \
        --description "${program} - ${description}" \
        --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
        --url "${url}" \
        --prefix /usr/bin \
        "${program}"
done

chmod 640 "${script_dir}"/.packaging/zfs-autobackup@.service

fpm \
    --force \
    --log "${fpm_loglevel:-error}" \
    --architecture all \
    --license "MIT" \
    --vendor "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --input-type dir \
    --output-type deb \
    --package "${distribution}/zfs-autobackup-systemd_${version}+${patchlevel}_all.deb" \
    --name zfs-autobackup-systemd \
    --version "${version}+${patchlevel}" \
    --description "systemd unit for zfs-autobackup" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://gitlab.com/packaging/zfs-autobackup" \
    --prefix /usr/lib/systemd/system/ \
    -C "${script_dir}"/.packaging \
    zfs-autobackup@.service
