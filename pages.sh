#!/bin/ash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

mkdir -p "${CI_PROJECT_DIR}"/public

cp -r \
    "${CI_PROJECT_DIR}"/.repo/gpg.key \
    "${CI_PROJECT_DIR}"/.repo/zfs-autobackup*/dists \
    "${CI_PROJECT_DIR}"/.repo/zfs-autobackup*/pool \
    "${CI_PROJECT_DIR}"/public/
