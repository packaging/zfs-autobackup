[[_TOC_]]

# [zfs-autobackup](https://github.com/psy0rz/zfs_autobackup) Packages

## What it is

* apt repo with packages
* statically compliled to binary using [Nuitka](https://github.com/Nuitka/Nuitka) - no python/virtualenv required

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-zfs-autobackup.asc https://packaging.gitlab.io/zfs-autobackup/gpg.key
```

## Add repo to apt

### Stable

```bash
echo "deb https://packaging.gitlab.io/zfs-autobackup zfs-autobackup-stable main" | sudo tee /etc/apt/sources.list.d/morph027-zfs-autobackup-stable.list
```

### Testing

```bash
echo "deb https://packaging.gitlab.io/zfs-autobackup zfs-autobackup-testing main" | sudo tee /etc/apt/sources.list.d/morph027-zfs-autobackup-testing.list
cat >/etc/apt/preferences.d/zfs-autobackup-testing <<EOF
Package: *
Pin: release o=morph027,n=zfs-autobackup-testing
Pin-Priority: 10
EOF
```

To install from testing: `apt-get -t n=zfs-autobackup-testing install zfs-autobackup`

## Systemd timer

* install `zfs-autobackup-systemd`
* create a timer which suits your needs like the following example (which runs hourly)

```
# /etc/systemd/system/zfs-autobackup@.timer
[Unit]
Description=zfs-autobackup for %i

[Timer]
Unit=zfs-autobackup@%i.service
OnBootSec=5min
OnUnitActiveSec=60min

[Install]
WantedBy=timers.target
```

* enable timer: `systemctl enable --now zfs-autobackup@source.timer` where source is the group name (e.g. `offsite1`)
* example: `systemctl enable --now zfs-autobackup@offsite1.timer`
* create a file `/etc/default/zfs-autobackup-source` and add `TARGET=target` (and optionally `OPTS=`)
* example: `echo -e "TARGET=data/backup/pve01\nOPTS=--zfs-compressed --buffer 1G --no-progress -v" > /etc/default/zfs-autobackup-offsite1`
